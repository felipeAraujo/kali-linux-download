# kali-linux-download

The purpose of this project is to download and check all checksums to get the Kali Linux in a reliable way.

# configuration

This script will run in a virtualenv. This will do the following command to start:
```
virtualenv -q --python={path-to-python3} python-code
```

After that you need to activate your virtualenv and install the pip requirements after, with the following commands:
```
source python-code/bin/activate
cd python-code
pip install -r requirements.txt
```

Run the ScriptDownload to download the kali image
You can change the url and the checksum in the .env file
```
python ScriptDownload.py
```

In the middle of script
![In the middle of download](images/middle_download.png)

When the script finishes
![finished script](images/complete_download.png)