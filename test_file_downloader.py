import unittest
import os
from unittest import mock
from FileDownloader.FileDownloader import FileDownloader
from requests import Request
import hashlib

class myMockedHeader:
    def get(self, string):
        return 10

class myMockedClass:
    def __init__(self):
        self.content = 10
        self.headers = myMockedHeader()
        self.count = 0
    
    def iter_content(self, chunk_size):
        if self.count == 0:
            self.count = self.count + 1
            return ['some body content yes'.encode('utf-8')]
        return []



def mocked_requests_get(*args, **kwargs):
    return myMockedClass()

class Download_Test_Case(unittest.TestCase):

    @mock.patch('requests.get', side_effect=mocked_requests_get)
    def test_generic_download_with_check_sum(self, mock_get):
        hash_sha256 = hashlib.sha256()
        hash_sha256.update('some body content yes'.encode('utf-8'))
        check_sum = hash_sha256.hexdigest()

        download_file = FileDownloader('temp/url', check_sum)
        download_file.download()

        self.assertEqual(check_sum, download_file.check_sum)
        os.remove('url')


if __name__ == '__main__':
    unittest.main()