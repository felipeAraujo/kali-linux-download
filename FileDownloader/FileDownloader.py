import os
import requests
import hashlib

from sys import stdout

class FileDownloader:
    def __init__(self, url_to_download = '', expected_check_sum = None, output_path = None):
        self.url_to_download = url_to_download

        self.output_path = output_path
        if self.output_path is None:
            self.output_path = os.getcwd()

        self.expected_check_sum = expected_check_sum

    def download(self):
        request = requests.get(self.url_to_download, stream = True)
        total_length = int(request.headers.get('content-length'))

        total_human = round(total_length/(1024*1024), 2)

        file_name = os.path.basename(self.url_to_download)
        with open(file_name, "wb") as fd:
            if total_length is None:
                fd.write(request.content)
                return

            till_now = 0
            hash_sha256 = hashlib.sha256()
            for data in request.iter_content(chunk_size=4096):
                till_now += len(data)
                current_human = round(till_now/(1024*1024), 2)

                fd.write(data)
                hash_sha256.update(data)

                screen_width = 25

                done = int(screen_width * till_now/total_length)

                stdout.write("\r[%s%s] %s/%s(M)" % ('=' * done, ' ' * (screen_width - done), current_human, total_human))
                stdout.flush()
            self.check_sum = hash_sha256.hexdigest()
            self._final_check_sum(self.check_sum, file_name)

    def _final_check_sum(self, check_sum, filename):
        is_valid = (self.expected_check_sum == None) or self.expected_check_sum == check_sum

        if is_valid:
            print("\nChecksum match, file OK")
            return True

        print("\nthe checksum didn't match. So the file will be deleted...")
        os.remove(filename)
        return False

