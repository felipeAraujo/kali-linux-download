import os
from FileDownloader.FileDownloader import FileDownloader
import settings

def downloadFile():
    url = os.getenv('DOWNLOAD_URL')
    check_sum = os.getenv('CHECK_SUM')

    fileDownloader = FileDownloader(url, check_sum)
    fileDownloader.download()

if __name__ == "__main__":
    # execute only if run as a script
    downloadFile()